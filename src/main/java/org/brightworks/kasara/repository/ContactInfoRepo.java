package org.brightworks.kasara.repository;

import org.brightworks.kasara.model.ContactInfo;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author kyel on 5/27/2015.
 */
public interface ContactInfoRepo extends PagingAndSortingRepository<ContactInfo,Long>{
}
