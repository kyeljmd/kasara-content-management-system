package org.brightworks.kasara.repository;

import org.brightworks.kasara.model.Page;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author kyel on 5/27/2015.
 */
public interface PageRepo extends PagingAndSortingRepository<Page,Long>{
}
