package org.brightworks.kasara.service.impl;

import org.brightworks.kasara.model.ContactInfo;
import org.brightworks.kasara.repository.ContactInfoRepo;
import org.brightworks.kasara.service.ContactInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author kyeljohndavid on 5/25/2015.
 */
@Service
public class ContactInfoServiceImpl implements ContactInfoService{

    @Autowired
    private ContactInfoRepo contactInfoRepo;

    @Override
    public ContactInfo get(Long id) {
        return contactInfoRepo.findOne(id);
    }

    @Override
    public ContactInfo save(ContactInfo contactInfo) {
        return contactInfoRepo.save(contactInfo);
    }
}
