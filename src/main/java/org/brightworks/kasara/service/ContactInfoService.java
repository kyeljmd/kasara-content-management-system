package org.brightworks.kasara.service;

import org.brightworks.kasara.model.ContactInfo;

/**
 * @author kyeljohndavid on 5/25/2015.
 */
public interface ContactInfoService {

    ContactInfo get(Long id);

    ContactInfo save(ContactInfo contactInfo);
}
