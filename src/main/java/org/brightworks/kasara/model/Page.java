package org.brightworks.kasara.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * @author kyel on 5/25/2015.
 */
@Entity
@Table(name="TXN_PAGE")
public class Page  extends  JpaModel{

    /**
     * Page Header message of the webapp
     */
    @Column(name = "PAGE_HEADER")
    private String pageHeader;

    @OneToMany
    private List<Section> sections;

    public String getPageHeader() {
        return pageHeader;
    }

    public void setPageHeader(String pageHeader) {
        this.pageHeader = pageHeader;
    }

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

    public boolean isSectionEmpty(){
        return (sections == null) ? true : sections.isEmpty();
    }
}
