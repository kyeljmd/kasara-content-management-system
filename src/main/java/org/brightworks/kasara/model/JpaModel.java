package org.brightworks.kasara.model;

import javax.persistence.*;

/**
 * @author kyel on 5/17/2015.
 */
@MappedSuperclass
public class JpaModel {

    /**
     * Generator name.
     */
    public static final String GENERATOR_NAME = "DEFAULT_ID_GEN";


    /**
     * Table name of sequence.
     */
    public static final String TABLE_NAME = "SEQ_ENTITY_ID";

    /**
     * Id initial value.
     */
    public static final int ID_INITIAL_VALUE = 1000;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = GENERATOR_NAME)
    @TableGenerator(name = GENERATOR_NAME, table = TABLE_NAME, initialValue = ID_INITIAL_VALUE)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
