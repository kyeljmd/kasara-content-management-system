package org.brightworks.kasara.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author by kyel on 5/17/2015.
 */
@Entity
@Table(name="TXN_SECTION")
public class Section extends JpaModel{

    @Column(name = "HEADING")
    private String heading;

    @Column(name = "CONTENT")
    private String content;

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
