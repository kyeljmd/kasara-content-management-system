package org.brightworks.kasara.controller;

import org.brightworks.kasara.service.ContactInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author by kyel on 5/17/2015.
 */
@Controller
public class IndexController {

    @Autowired
    private ContactInfoService contactInfoService;

    @RequestMapping("/")
    public ModelAndView index(){
        ModelAndView mav = new ModelAndView("index");
        return mav;
    }

}
